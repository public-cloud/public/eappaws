This has three parts, directions, shell script of AWS Code and a stub of terraform to do a short clean up.

Tasks:
configure new AWS accounts,
    - set roles as full admins
    - setup MFA for all accounts
configure CloudTrail and storage bucket
    - S3 bucket for storage
    - life cycle (keep for 400 days, version, encrypted, make WORM?)
    - Set CloudTrail to use bucket
setup GuardDuty in application region
    - use template to only send high risk events to SNS channel (use existing) - CloudWatch Metric with alert transform
route53 health check with SNS notification (using existing channel)
