[[_TOC_]]

# Overview 

## Summary of Examples
- **eApp-instance-s3.json** - Starter Template for access to s3 buckets and folders

# Tutorial
![Creating IAM Access to S3](Creating IAM Access to s3.gif)
## Create IAM Policy
### Edit policy template

#### "Sid": "AllowListingOfImagesBucket"
- Update the list of [ARNs](https://docs.aws.amazon.com/general/latest/gr/aws-arns-and-namespaces.html) in the **"Resource"** element list to match the s3 buckets of the environment 
- Update the conditions section by choosing one of the following:
    - Change *environment* to the subfolder of the bucket to which access is needed
    - Remove the **"Condition"** section if giving access to entire bucket

#### "Sid": "AllowAllS3ActionsInUserFolder"
- Update the list of [ARNs](https://docs.aws.amazon.com/general/latest/gr/aws-arns-and-namespaces.html) in the **"Resource"** element list to match the s3 buckets of the environment 
    - Include or remove the subfolder paths as needed
    - Ensure the ARN ends with slash star /* 
        - This allows access to the contents of the bucket
### Add policy to AWS IAM
- Go to the **IAM** service in the AWS Console
- Choose **Policies** from the left navigation pane
- Click the **Create policy** button
- Click the **JSON** tab
- Paste the JSON policy edited in previous section into field
- Choose the **Next: Tags** button toward the bottom of the page
- Add ***Tags*** (optional)
- Choose the **Next: Review** button towards the bottom of the page
- Add a ***Name*** 
    - *Examples:* 
        - *appname-environment-service-purpose* 
        - *eapp-dev-s3-images*
        - *eapp-prod-s3-logs*
- Add a ***Description*** (optional)
- Choose the **Create policy** button at the bottom of the page


## Create IAM Role
- Go to the **IAM** service in the AWS Console
- Choose **Roles** from the left navigation pane
- Click the **Create role** button
- Select **EC2** under Choose Use Case
- Choose the **Next: Permissions** button towards the bottom of the page
- Search for the policy name created in the **Add policy to AWS IAM** section of this document
- Check the box next to the policy to select the policy
- Search for **AmazonSSMManagedInstanceCore** (used to allow SSM Agent to patch server)
- Check the box next to the policy to select the policy
- Choose the **Next: Tags** button toward the bottom of the page
- Add ***Tags*** (optional)
- Choose the **Next: Review** button towards the bottom of the page
- Add a ***Name*** 
    - *Example:*
        - *appname-environment-serverfunction*
        - *eapp-dev-frontend*
        - *eapp-prod-apiserver*
- Add a ***Description*** (optional)
- Choose the **Create role** button at the bottom of the page

## Attach Role to Instance
- Go to the **EC2** service in the AWS Console
- Choose **Instances** from the left navigation pane
- Click on the instance which needs the role
- Select **Actions** dropdown near to top of the EC2 console.
- Select **Security > Modify IAM role** from the dropdown list
- Search for the role name created in the **Create IAM Role** step
- Select the role
- Choose the **Save** button at the bottom of the page

# Testing
## Windows
- Open PowerShell  as admin or command line from the Windows EC2 instance
- Run the following [command](https://docs.aws.amazon.com/cli/latest/reference/s3/ls.html) in PowerShell  or command line *aws s3 ls s3://mybucket*
## Linux
- Run the following [command](https://docs.aws.amazon.com/cli/latest/reference/s3/ls.html) *aws s3 ls s3://mybucket*

# Troubleshooting
## Invalid JSON
- Use [JSONLint.com](https://jsonlint.com/) to validate the JSON
- Check for proper placement of commas in lists

## Instance cannot Access S3 bucket
- Ensure the Resource ARNs in "Sid": "AllowAllS3ActionsInUserFolder" end with /*
- Run the following [command](https://docs.aws.amazon.com/cli/latest/reference/s3/ls.html) in PowerShell or command line *aws s3 ls s3://mybucket* and use the output to troubleshoot